// entry point of the file
import React from 'react'
import ReactDOM from 'react-dom'

// creating APP component
function APP (props){
    return(
        <div>
            <h1>React App - from scratch...</h1>
            <a href="https://youtu.be/TRXV8I7ss3Q" target="_blank">Youtube - by "Kode any" - create a react app from scratch with webpack-4 | No CLI - 2021</a>
        </div>
    )
}

ReactDOM.render(
    <APP/>,
    document.getElementById('root')
)